MapVar("VisualizeLOD_Objs", false)
function ToggleVisualizeLOD()
	local GetCurrentLOD = CObject.GetCurrentLOD
	local SetColorModifier = CObject.SetColorModifier
	local GetColorModifier = CObject.GetColorModifier
	local IsValid = IsValid
	local LOD_colors = { red, green, blue, yellow, magenta, cyan, orange }
	local versions = {}
	if VisualizeLOD_Objs then
		SuspendThreadDebugHook("VisualizeLOD")
		for obj, cm in pairs(VisualizeLOD_Objs) do
			if IsValid(obj) then
				SetColorModifier(obj, cm)
			end
		end
		ResumeThreadDebugHook("VisualizeLOD")
		VisualizeLOD_Objs = false
		return
	else
		VisualizeLOD_Objs = {}
		CreateMapRealTimeThread(function()
			local from, to, last_eye, last_lookat
			local mw, mh = terrain.GetMapSize()
			local max_dist = Min(mw, mh)
			local width = 256*guim
			local screen = box(point20, UIL.GetScreenSize())
			local Point2DInside = screen.Point2DInside
			local version = 0
			local function ProcessObj(obj, version, versions)
				local front, pos = GameToScreen(obj)
				if front and Point2DInside(screen, pos) then
					versions[obj] = version
				end
			end
			while VisualizeLOD_Objs  do
				SuspendThreadDebugHook("VisualizeLOD")
				local eye, lookat = GetCamera()
				if from ~= eye or last_lookat ~= lookat then
					to = terrain.IntersectRay(eye, lookat)
					if not to then
						to = eye + SetLen(lookat - eye, max_dist)
					end
					from = eye
					last_lookat = lookat
				end
				version = version + 1 
				MapForEach(from, to, width, ProcessObj, version, versions)
				local orig_colors = VisualizeLOD_Objs
				for obj, obj_version in pairs(versions) do
					if obj_version ~= version then
						local orig_color = orig_colors[obj]
						if orig_color then
							if IsValid(obj) then
								SetColorModifier(obj, orig_color)
							end
							orig_colors[obj] = nil
						end
						versions[obj] = nil
					else
						local lod = GetCurrentLOD(obj) or -1
						local color = lod >= 0 and LOD_colors[lod + 1]
						if color then
							orig_colors[obj] = orig_colors[obj] or GetColorModifier(obj)
							SetColorModifier(obj, color)
						end
					end
				end
				ResumeThreadDebugHook("VisualizeLOD")
				Sleep(50)
			end
		end)
		return true
	end
end

local orig_ToggleHR = ToggleHR
function ToggleHR( param )
	if param == "VisualizeLOD" then
		return ToggleVisualizeLOD()
	end
	return orig_ToggleHR( param )
end

----

if FirstLoad then
	DbgClearLast = false
	DbgZTest = false
end
MapVar("DbgObjects", false)

function DbgClearObjs()
    DoneObjects(DbgObjects)
    DbgObjects = false
end

if const.DebugRender then
	OnMsg.LoadGame = DbgClearObjs
	return
end

----

function DbgAddObj(obj)
	obj:SetDepthTest(DbgZTest)
	DbgObjects = table.create_add_unique(DbgObjects, obj)
	--return obj
end

--To make dbg clear work when hitting F9:
function DbgClear(once)
    if once then
        local time = RealTime()
        if DbgClearLast == time then
            return
        end
        DbgClearLast = time
    end
    DbgClearObjs()
    Msg("DbgClear")
end
OnMsg.ChangeMap = DbgClear
OnMsg.LoadGame = DbgClear

function DbgSetVectorZTest(enable)
	enable = enable or false
	if enable == DbgZTest then return end
	DbgZTest = enable
	table.validate(DbgObjects)
	for _, obj in ipairs(DbgObjects) do
		obj:SetDepthTest(enable)
	end
end

--- Draw a text
-- @cstyle void DbgAddText(string text, point pos_or_obj, int color = RGB(255, 255, 255), string font_face = const.SystemFont)
function DbgAddText(text, pos_or_obj, color, font_face)
	local dbg_obj = PlaceObject("Text")
	if pos_or_obj then
		dbg_obj:SetPos(ResolvePoint(pos_or_obj))
	end
	if font_face then
		dbg_obj:SetFontId(font_face)
	end
	if IsT(text) then
		text = TTranslate(text, pos_or_obj)
	end
	dbg_obj:SetText(tostring(text))
	if color then
		dbg_obj:SetColor(color)
	end
	return DbgAddObj(dbg_obj)
end

--- Draw a debug polygon
-- @cstyle void DbgAddPoly(point poly[], int color = RGB(255, 255, 255), bool dont_close = false)
function DbgAddPoly(poly, color)
	local pts = {}
	for _, pt in ipairs(poly) do
		if IsValidPos(pt) then
			pts[#pts + 1] = ResolvePoint(pt)
		end
	end
	return DbgAddObj(PlacePolyLine(pts, color or white))
end

--- Draw a debug vector
-- @cstyle void DbgAddVector(point origin, point vector, int color = RGB(255, 255, 255))
function DbgAddVector(origin, vector, color)
	local pos = ResolvePoint(origin)
	vector = vector or point(0, 0, 10*guim)
	return DbgAddPoly({pos, pos + vector}, color)
end

--- Draw a debug segment between two points
-- @cstyle void DbgAddSegment(point from, point to, int color = RGB(255, 255, 255))
function DbgAddSegment(from, to, color)
	return DbgAddPoly({from, to}, color)
end

--- Draw a debug triangle
-- @cstyle void DbgAddTriangle(point pt0, point pt1, point pt2, int color = RGB(255, 255, 255))
function DbgAddTriangle(pt0, pt1, pt2, color)
	return DbgAddPoly({pt0, pt1, pt2, pt0}, color)
end

--- Draw a debug circle
-- @cstyle void DbgAddCircle(point center, int radius, int color = RGB(255, 255, 255))
function DbgAddCircle(center, radius, color)	
	return DbgAddObj(PlaceCircle(center, radius, color or white))
end

--- Draw a debug circle
-- @cstyle void DbgAddSphere(point center, int radius, int color = RGB(255, 255, 255))
function DbgAddSphere(center, radius, color)	
	return DbgAddObj(PlaceSphere(center, radius, color or white))
end

--- Draw a debug box
-- @cstyle void DbgAddBox(box box, int color = RGB(255, 255, 255))
function DbgAddBox(box, color)
	return DbgAddObj(PlaceBox(box, color or white))
end