local function __SearchTs(tbl, Ts, visited, depth)
	if visited[tbl] then return end
	visited[tbl] = tbl
	for key, value in sorted_pairs(tbl) do
		if type(value) == "table" then
			if getmetatable(value) == TMeta then
				if type(value[1]) ~= "number" then
					print("Missing translation ID: " .. TableToLuaCode(value))
				else
					Ts[value[1]] = value
				end
			elseif depth > 0 then
				__SearchTs(value, Ts, visited, depth - 1)
			end
		end
	end
end

function ExtractTranslation(mod_title_or_id)
	mod_title_or_id = mod_title_or_id or ""
	local mod_found
	for id, mod in pairs(Mods) do
		if mod.title == mod_title_or_id or mod.id == mod_title_or_id then
			mod_found = mod
			break
		end
	end
	if not mod_found then
		print("Mod " .. mod_title_or_id .. " not found!")
		return 
	end
	print("Mod found: Title '" .. mod_found.title .. "', ID '" .. mod_found.id .. "'")
	local Ts, visited = {}, {}
	__SearchTs(mod_found, Ts, visited, 0)
	for _, item in ipairs(mod_found.items) do
		__SearchTs(item, Ts, visited, 1)
	end
	for _, option in ipairs(mod_found.options) do
		__SearchTs(option, Ts, visited, 1)
	end
	return Ts
end