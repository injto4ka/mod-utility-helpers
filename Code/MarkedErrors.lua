if not Platform.debug or MapVarValues.MarkedErrors then return end

MapVar("MarkedErrors", {})
PersistableGlobals.MarkedErrors = false

function ClearErrorSource(x)
	if not MarkedErrors then return end
	local count = #MarkedErrors
	if not x then
		table.iclear(MarkedErrors)
	else
		for i=#MarkedErrors,1,-1 do
			if MarkedErrors[i].obj == x or MarkedErrors[i].pos == x then
				table.remove(MarkedErrors, i)
			end
		end
	end
	if count ~= #MarkedErrors then
		DelayedCall( 100, ObjModified, MarkedErrors )
	end
end

local function PrintPreset(tbl)
	return tbl[1] .. "." .. tbl.group .. "." .. tbl.id
end

local function UnserializePreset(tbl)
	-- the right-hand side of the 'or' handles FXPreset, which doesn't have an id
	return table.get(Presets, tbl[1], tbl.group, tbl.id) or table.get(Presets, table.unpack(tbl))
end

local function SerializePreset(preset)
	local idx1, idx2 = unpack_params(PresetGetPath(preset)) -- store indexes because of FXPreset that doesn't have an id
	return { preset.PresetClass or preset.class, idx1, idx2, group = preset.group, id = preset:GetIdentification()  }
end

local function ComparePreset(orig, tbl)
	return orig and IsKindOf(tbl, "Preset") and table.iequal(orig, SerializePreset(tbl))
end

local function StoreErrorSourceEx(severity, silent, source, msg, ...)
	if not MarkedErrors then
		return ""
	end
	local prev_err
	local idx = 1+#MarkedErrors
	for i, err in ipairs(MarkedErrors) do
		if source and (err.obj == source or err.pos == source or ComparePreset(err.preset, source) or err.xtext_err == source) then
			idx = i
			prev_err = err
			break
		end
	end
	local report_time = RealTime()
	local warning = severity == "warning"
	local map = GetMapName()
	msg = msg and print_format(msg, ...) or nil
	local msg_exists = msg and prev_err and (prev_err.msg == msg or string.find(prev_err.msg, msg, 1, true))
	if not msg_exists and prev_err and report_time == prev_err.report_time then
		msg = prev_err.msg .. " | " .. msg
	end
	local msg_start = warning and " Warning: " or " Error: "
	local msg_print = msg and (msg_start .. msg) or ""
	local msg_res, err_obj
	if IsPoint(source) then
		err_obj = MarkedError:new{ pos = source, msg = msg, map = map, warning = warning }
		msg_res = string.format("VME(%d) = %s%s", idx, tostring(source), msg_print)
	elseif IsValid(source) then
		local pos = source:IsValidPos() and tostring(source:GetPos()) or "(invalid)"
		local id = source:HasMember("id") and source.id
		local id_str = id and string.format(" '%s'", id) or ""
		local handle = source:HasMember("handle") and type(source.handle) == "number" and source.handle
		local handle_str = handle and string.format(" [%d]", handle) or ""
		err_obj = MarkedError:new{ obj = source, pos = source:GetPos(), id = id, handle = handle, msg = msg, map = map, warning = warning }
		msg_res = string.format("VME(%d) = %s%s%s at %s%s", idx, source.class, id_str, handle_str, pos, msg_print)
	elseif IsKindOf(source, "Preset") then
		err_obj = MarkedError:new{ preset = SerializePreset(source), msg = msg, warning = warning }
		msg_res = string.format("VME(%d) %s, id = %s%s", idx, source.class, source:GetIdentification(), msg_print)
	elseif IsKindOf(source, "XTextParserError") then
		err_obj = MarkedError:new{ xtext_err = source, msg = msg, warning = warning }
		msg_res = string.format("VME(XText) %s", msg_print)
	else
		err_obj = MarkedError:new{ msg = msg, warning = warning }
		msg_res = string.format("VME(invalid) %s", msg_print)
	end
	MarkedErrors[idx] = err_obj
	err_obj.source = source
	err_obj.report_time = report_time
	err_obj.game_time = GameTime()
	
	if not msg_exists and not silent and not config.SilentVME then
 		DelayedCall(100, OpenVMEViewer)
	end
	ObjModified(MarkedErrors)
	return msg_res
end

DefineClass.MarkedError = {
	__parents = {"PropertyObject"},
	obj = false,
	pos = false,
	id = false,
	handle = false,
	msg = false,
	map = false,
	preset = false,
	xtext_err = false,
	warning = false,
	source = false,
	report_time = false,
}

function MarkedError:GetEditorView()
	local msg_res = self.msg and (" Error: " .. self.msg) or ""
	local pos = self.pos
	local idx = table.find(MarkedErrors, self) or -1
	local color = self.warning and "<color 255 140 0>" or "<color 240 0 0>"
	if self.preset then
		return string.format("VME(%d) %sPreset%s", idx, color, msg_res)
	elseif not IsPoint(pos) then
		return string.format("VME(%d) %s%s", idx, color, msg_res)
	elseif self.obj then
		local pos_str = pos == InvalidPos() and "Invalid pos" or tostring(pos)
		return string.format("VME(%d) %s%s at %s%s", idx, color, self.obj.class, pos_str, msg_res)
	else
		return string.format("VME(%d) %s%s%s", idx, color, tostring(pos), msg_res)
	end
end

function MarkedError:OnEditorSelect(selected)
	local idx = selected and table.find(MarkedErrors, self)
	if idx then
		VME(idx)
	end
end

function StoreErrorSource(source, msg, param1, ...)
	if source == "silent" then
		return StoreErrorSourceEx("error", "silent", msg, param1, ...)
	else
		return StoreErrorSourceEx("error", not "silent", source, msg, param1, ...)
	end
end

function StoreWarningSource(source, msg, param1, ...)
	if source == "silent" then
		return StoreErrorSourceEx("warning", "silent", msg, param1, ...)
	else
		return StoreErrorSourceEx("warning", not "silent", source, msg, param1, ...)
	end
end

function OnMsg.PreSaveMap()
	ClearErrorSource()
end

if FirstLoad then
	GedVMEViewerInstance = false
end

function OpenVMEViewer()
	if Platform.editor and not Platform.console then
		CreateRealTimeThread(function()
			if not GedVMEViewerInstance or not GedConnections[GedVMEViewerInstance.ged_id] then
				GedVMEViewerInstance = OpenGedApp("GedVMEViewer", MarkedErrors) or false
			end
		end)
	end
end

local function ClassAndId(obj)
	local ret = obj.class
	local id_str = obj.id and string.format(" '%s'", obj.id) or ""
	local handle_str = obj.handle and string.format(" [%d]", obj.handle) or ""
	return ret .. id_str .. handle_str
end

function VME_ViewPos_Editor(obj, pos)
	local dist
	if IsValid(obj) then
		editor.ClearSel()
		editor.AddToSel{obj}
		OpenGedGameObjectEditor(editor.GetSel())
		dist = 2 * ObjectHierarchyBSphere(obj)
	end
	ViewPos(pos, dist, "cameraMax")
end

function VME_ViewPos_Game(obj, pos)
	if IsValid(obj) and obj:GetEnumFlags(const.efSelectable) ~= 0 then
		ViewAndSelectObject(obj)
	else
		ViewPos(pos)
	end
end

function VME(idx)
	local x = MarkedErrors and MarkedErrors[idx] or {}
	local preset_info = x.preset
	if preset_info then
		local preset = UnserializePreset(preset_info)
		if not preset then
			print("No such preset: " .. PrintPreset(preset_info))
		else
			CreateRealTimeThread(function()
				local ged = OpenPresetEditor(preset.PresetClass or preset.class, preset:EditorContext())
				if ged then
					ged:SetSelection("root", PresetGetPath(preset))
				end
			end)
		end
		return
	end
	
	local pos = x.pos
	local obj = x.obj
	local map = x.map

	if map and map ~= GetMapName() then
		if not CanYield() then
			return CreateRealTimeThread(VME, idx)
		end
		if WaitQuestion(terminal.desktop, 
			Untranslated("Warning"),
			Untranslated("Change map to view the error source?"),
			Untranslated("Change map"),
			Untranslated("Cancel")) == "cancel" then
			return
		end
		ChangeMap(map)
	end
	if obj and not IsValid(obj) and pos ~= InvalidPos() then
		local objs = MapGet(pos, 0, obj.class)
		if #(objs or "") == 1 then
			obj = objs[1]
		end
	end
	if IsValid(obj) then
		pos = obj:GetPos()
		local pos_str = pos == InvalidPos() and "Invalid pos" or tostring(pos)
		printf("VME(%d) = %s at %s", idx, ClassAndId(x), pos_str)
	elseif obj then
		printf("VME(%d) = destroyed %s at %s", idx, ClassAndId(x), tostring(pos))
	else
		printf("VME(%d) = %s", idx, tostring(pos or "no position"))
	end
	if not IsEditorActive() then
		VME_ViewPos_Game(obj, pos)
	else
		VME_ViewPos_Editor(obj, pos)
	end
end

function OnMsg.PreDeletedCObjectsCheck()
	for _, err in ipairs(MarkedErrors) do
		ValidateMember(err, "obj")
		ValidateMember(err, "source")
	end
end