MapVar("ObjectRefs", false)
PersistableGlobals.ObjectRefs = false

function GetObjectByHandle(handle)
	if not handle then
		return
	end
	if handle >= 0 then
		return HandleToObject[handle]
	else
		return ObjectRefs[-handle]
	end
end

function ExtractHierarchyInfo(obj, visited)
	obj = obj or selo() or SelectedObj
	if not IsValid(obj) then
		return
	end
	visited = visited or {}
	local data = visited[obj]
	if data then
		return data
	end
	local obj_meta = {
		__tostring = function(v)
			return string.format("%s (%d)", v.classname, v.handle or 0)
		end,
	}
	data = setmetatable({}, obj_meta)
	visited[obj] = data
	data.classname = obj.class
	if not obj.handle then
		local refs = ObjectRefs or {}
		refs[#refs + 1] = obj
		ObjectRefs = refs
		obj.handle = -(#ObjectRefs)
	end
	data.handle = obj.handle
	data.transform_pos = obj:GetPos()
	data.transform_angle = obj:GetAngle()
	data.transform_axis = obj:GetAxis()
	data.transform_scale = obj:GetScale()
	
	local function AddInfoToList(obji, list)
		if not IsValid(obji) then return end
		list[#list + 1] = ExtractHierarchyInfo(obji, visited)
	end
	local parent = obj:GetParent()
	if parent then
		data.attached_to = ExtractHierarchyInfo(parent, visited)
		data.attach_spot_idx = obj:GetAttachSpot()
		data.attach_spot_name = parent:GetSpotName(obj:GetAttachSpot())
	end
	local objects_meta = {
		__tostring = function(v)
			return string.format("%d %s", #v, #v == 1 and "object" or "objects")
		end,
	}
	local attaches = setmetatable({}, objects_meta)
	obj:ForEachAttach(AddInfoToList, attaches)
	if next(attaches) then
		data.attaches = attaches
	end
	
	local container = obj.container
	if IsValid(container) then
		data.container = ExtractHierarchyInfo(container, visited)
	end
	local elems = setmetatable({}, objects_meta)
	for _, elem in ipairs(obj.elems) do
		AddInfoToList(elem, elems)
	end
	if next(elems) then
		data.contained_elems = elems
	end
	
	local supported_by = setmetatable({}, objects_meta)
	for _, support in ipairs(obj.supported_by) do
		AddInfoToList(support, supported_by)
	end
	if next(supported_by) then
		data.supports = supported_by
	end
	
	local supported_objs = setmetatable({}, objects_meta)
	for _, support in ipairs(obj.supported_objs) do
		AddInfoToList(support, supported_objs)
	end
	if next(supported_objs) then
		data.supported_objs = supported_objs
	end

	local collection_idx = obj:GetCollectionIndex()
	if collection_idx ~= 0 then
		data.collection_idx = collection_idx
	end
	local child_objs = setmetatable({}, objects_meta)
	if obj.GetChildObjects then
		for _, child in ipairs(obj:GetChildObjects()) do
			AddInfoToList(child, child_objs)
		end
	end
	if next(child_objs) then
		data.child_objs = child_objs
	end

	return data
end