if FirstLoad then
	ArtSpec_Preorder = false
end

local function ApplyArtSpec()

if ArtSpec_Preorder then return end
ArtSpec_Preorder = true

PlaceObj('EntitySpec', {
	editor_exclude = true,
	editor_tags = {
		Door = true,
	},
	fade_category = "Never",
	group = "Default",
	id = "Door_Planks_Single_01",
	last_change_time = 1657543198,
	material_type = "Logs",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "broken",
		'lod', 2,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('MeshSpec', {
		'name', "meshAnim",
		'lod', 2,
		'animated', true,
	}),
	PlaceObj('MeshSpec', {
		'name', "meshAnim_Broken",
		'lod', 2,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "broken",
		'mesh', "broken",
	}),
	PlaceObj('StateSpec', {
		'name', "closing",
		'mesh', "meshAnim",
	}),
	PlaceObj('StateSpec', {
		'name', "closingInside",
		'mesh', "meshAnim",
	}),
	PlaceObj('StateSpec', {
		'name', "closingInside_Broken",
		'mesh', "meshAnim_Broken",
	}),
	PlaceObj('StateSpec', {
		'name', "closing_Broken",
		'mesh', "meshAnim_Broken",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "opening",
		'mesh', "meshAnim",
	}),
	PlaceObj('StateSpec', {
		'name', "openingInside",
		'mesh', "meshAnim",
	}),
	PlaceObj('StateSpec', {
		'name', "openingInside_Broken",
		'mesh', "meshAnim_Broken",
	}),
	PlaceObj('StateSpec', {
		'name', "opening_Broken",
		'mesh', "meshAnim_Broken",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	editor_tags = {
		Floor = true,
	},
	fade_category = "Never",
	group = "Default",
	id = "Floor_Planks",
	last_change_time = 1584350644,
	material_type = "Logs",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	editor_tags = {
		None = true,
	},
	fade_category = "Never",
	group = "Default",
	id = "LandingPod_PreorderBonus",
	last_change_time = 1658215877,
	material_type = "Metal",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "lit",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "lit",
		'mesh', "lit",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Column_01",
	last_change_time = 1657616030,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Eave_01",
	last_change_time = 1657275896,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Eave_02",
	last_change_time = 1657275994,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Eave_03",
	last_change_time = 1657275998,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_GableCrest_01",
	last_change_time = 1690880715,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_GableCrest_02",
	last_change_time = 1690880722,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_GableSlope_01",
	last_change_time = 1690802202,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_GableSlope_02",
	last_change_time = 1690802210,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Gable_01",
	last_change_time = 1657275896,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Gable_02",
	last_change_time = 1657276003,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Plane_01",
	last_change_time = 1657275896,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Plane_02",
	last_change_time = 1657276006,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Plane_03",
	last_change_time = 1657276010,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Plane_04",
	last_change_time = 1657276010,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_RakeEave_01",
	last_change_time = 1657275896,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_RakeGableCrestBot_01",
	last_change_time = 1690880725,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_RakeGableCrestTop_01",
	last_change_time = 1690881539,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_RakeGableSlopeBot_01",
	last_change_time = 1690802192,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_RakeGableSlopeTop_01",
	last_change_time = 1690881556,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_RakeGable_01",
	last_change_time = 1657275896,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_RakeRidge_01",
	last_change_time = 1657275896,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Rake_01",
	last_change_time = 1657275896,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Rake_02",
	last_change_time = 1657276022,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Rake_03",
	last_change_time = 1657276029,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_RidgeCut_01",
	last_change_time = 1692096019,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Ridge_01",
	last_change_time = 1657275896,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Ridge_02",
	last_change_time = 1657276022,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Roof_Planks_Ridge_03",
	last_change_time = 1657276029,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "FloorAlignedObj",
	editor_exclude = true,
	editor_tags = {
		Stairs = true,
	},
	fade_category = "Never",
	group = "Default",
	id = "Stairs_Planks_01",
	last_change_time = 1609675715,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "Mirrorable",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Terrace_Wood_Stairs_01",
	last_change_time = 1629297315,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "Mirrorable",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Terrace_Wood_Wall_01",
	last_change_time = 1584354123,
	material_type = "Planks",
	save_in = "Preorder",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "Mirrorable",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Terrace_Wood_Wall_02",
	material_type = "Planks",
	save_in = "Preorder",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "Mirrorable",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Terrace_Wood_Wall_Broken_01",
	last_change_time = 1656961057,
	material_type = "Planks",
	save_in = "Preorder",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "Mirrorable",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Terrace_Wood_Wall_Broken_02",
	last_change_time = 1656961776,
	material_type = "Planks",
	save_in = "Preorder",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "Mirrorable",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Terrace_Wood_Wall_Broken_03",
	last_change_time = 1656961779,
	material_type = "Planks",
	save_in = "Preorder",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_CapL_01",
	last_change_time = 1584354524,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_CapT_01",
	last_change_time = 1584354520,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_CapX_01",
	last_change_time = 1584354516,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_Corner_01",
	last_change_time = 1584354511,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_Marker_01",
	material_type = "Planks",
	save_in = "Preorder",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_Wall_ExEx_01",
	last_change_time = 1584354506,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_Wall_ExEx_02",
	last_change_time = 1584354506,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_Wall_ExEx_03",
	last_change_time = 1584354506,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_Wall_ExEx_04",
	last_change_time = 1657188071,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_Wall_ExEx_Broken_01",
	last_change_time = 1656960860,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_Wall_ExEx_Broken_02",
	last_change_time = 1656960878,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WallExt_Planks_Wall_ExEx_Broken_03",
	last_change_time = 1656960881,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "WindowVent_Planks_Single_01",
	last_change_time = 1657543249,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "closed",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "meshAnim",
		'lod', 2,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "closed",
		'mesh', "closed",
	}),
	PlaceObj('StateSpec', {
		'name', "closing",
		'mesh', "meshAnim",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "opening",
		'mesh', "meshAnim",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Window_Planks_Single_01",
	last_change_time = 1657543269,
	material_type = "Planks",
	save_in = "Preorder",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "closed",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "meshAnim",
		'lod', 2,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "closed",
		'mesh', "closed",
	}),
	PlaceObj('StateSpec', {
		'name', "closing",
		'mesh', "meshAnim",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "opening",
		'mesh', "meshAnim",
	}),
})

end

OnMsg.DataPreprocess = ApplyArtSpec
OnMsg.ModsReloaded = ApplyArtSpec

