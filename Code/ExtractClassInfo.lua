function ExtractClassInfo(class)
	class = class or selo() or SelectedObj
	if type(class) == "table" then
		class = class.class
	end
	local classdef = g_Classes[class]
	if not classdef then
		return
	end
	local classes = table.keys(classdef.__ancestors, true)
	table.insert(classes, 1, class)
	return classes
end

function ExtractCommonAncestors(objs)
	objs = objs or next(Selection) and Selection or next(editor.GetSel()) and editor.GetSel() or empty_table
	if #objs == 0 then return end
	local ancestors = table.copy(objs[1].__ancestors)
	for i=2,#objs do
		local ancestors_i = objs[i].__ancestors
		for class in pairs(ancestors_i) do
			if not ancestors[class] then
				ancestors[class] = nil
			end
		end
		for class in pairs(ancestors) do
			if not ancestors_i[class] then
				ancestors[class] = nil
			end
		end
	end
	return table.keys(ancestors, true)
end