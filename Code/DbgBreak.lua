if FirstLoad then
    DbgBreakValue = false
end

function DbgWaitMessage(value)
    WaitMessage(nil, Untranslated("Debug"), Untranslated(ValueToStr(value))) -- show the message and wait
end

function DbgBreak(value, silent)
    print("BREAK")
    OutputDebugStringNL(ValueToLuaCode(value))
    LuaPrintLocals(1)
    OutputDebugStringNL(GetStack(2))
    DbgBreakValue = value
    Msg("OnDbgBreak", value)
    Pause("Debug") -- this is a real pause, not just setting the game speed to 0
    local paused = true
    local main_thread = CurrentThread()
    local unpause_thread = CreateRealTimeThread(function()
        WaitMsg("ChangeGameSpeed")
        paused = false
        Resume("Debug") -- unpause the game
        Wakeup(main_thread) -- notify the main thread to continue
    end)
    if not CanYield() then -- test if the code can be interrupted
        print("Cannot stop execution here!")
        if not silent then
            LaunchRealTimeThread(DbgWaitMessage, value) -- if not, execute in a thread instead
        end
        return
    end
    if not silent then
        DbgWaitMessage(value) -- wait user input
     end
     if paused and IsValidThread(unpause_thread) then
        WaitWakeup() -- wait unpause
    end
end

function OnMsg.DoneMap()
    DbgBreakValue = false
end