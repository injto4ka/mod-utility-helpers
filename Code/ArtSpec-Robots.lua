if FirstLoad then
	ArtSpec_Robots = false
end

local function ApplyArtSpec()

if ArtSpec_Robots then return end
ArtSpec_Robots = true

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "CrawlerTurret_Base",
	last_change_time = 1690541925,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh2",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "death",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "death_Idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "siege",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "sieged",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "sieged_Static",
		'mesh', "mesh2",
	}),
	PlaceObj('StateSpec', {
		'name', "unsieged",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "walk",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "CrawlerTurret_Telescope",
	last_change_time = 1691661961,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh2",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "contract",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "death",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "death_Idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "extend",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "extend_Static",
		'mesh', "mesh2",
	}),
	PlaceObj('StateSpec', {
		'name', "extended",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "walk",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "CrawlerTurret_Top",
	last_change_time = 1691661971,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "CrawlerTurret_WeaponMount",
	last_change_time = 1691661980,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Cyborg_M_Henry_Arms",
	inherit_entity = "HumanMale",
	last_change_time = 1693385245,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Cyborg_M_Henry_Feet",
	inherit_entity = "HumanMale",
	last_change_time = 1693385225,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Cyborg_M_Henry_Hands",
	inherit_entity = "HumanMale",
	last_change_time = 1693385236,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "HumanHead",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Cyborg_M_Henry_Head",
	inherit_entity = "HumanMale",
	last_change_time = 1693385182,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Cyborg_M_Henry_Pelvis",
	inherit_entity = "HumanMale",
	last_change_time = 1693393081,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Cyborg_M_Henry_Shoulders",
	inherit_entity = "HumanMale",
	last_change_time = 1693385204,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Cyborg_M_Henry_Tighs",
	inherit_entity = "HumanMale",
	last_change_time = 1693385216,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "RobotSkinnedDecal",
	editor_artset = "Common",
	editor_category = "Decal",
	editor_subcategory = "Blood",
	group = "Default",
	id = "DecRobotBloodSkinned_01",
	last_change_time = 1694425486,
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "RobotSkinnedDecal",
	editor_artset = "Common",
	editor_category = "Decal",
	editor_subcategory = "Blood",
	group = "Default",
	id = "DecRobotBloodSkinned_02",
	last_change_time = 1694425519,
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "RobotSkinnedDecal",
	editor_artset = "Common",
	editor_category = "Decal",
	editor_subcategory = "Blood",
	group = "Default",
	id = "DecRobotBloodSkinned_03",
	last_change_time = 1694425520,
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "DroidParts_01",
	last_change_time = 1692025998,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "DroidParts_02",
	last_change_time = 1692026560,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "DroidParts_03",
	last_change_time = 1692026565,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "DroidParts_04",
	last_change_time = 1692026565,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "DroidParts_05",
	last_change_time = 1692026570,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "DroidParts_06",
	last_change_time = 1692026570,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "DroidParts_07",
	last_change_time = 1692026570,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "DroidParts_08",
	last_change_time = 1692026570,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians",
	last_change_time = 1692779795,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "lit",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "fly",
		'mesh', "lit",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "land",
		'mesh', "lit",
	}),
	PlaceObj('StateSpec', {
		'name', "lit",
		'mesh', "lit",
	}),
	PlaceObj('StateSpec', {
		'name', "takeOff",
		'mesh', "lit",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_01",
	last_change_time = 1694165263,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_02",
	last_change_time = 1694165306,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_03",
	last_change_time = 1694165347,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_04",
	last_change_time = 1694165349,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_05",
	last_change_time = 1694165352,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_06",
	last_change_time = 1694165354,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_07",
	last_change_time = 1694165355,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_08",
	last_change_time = 1694165357,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_09",
	last_change_time = 1694165360,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_10",
	last_change_time = 1694165367,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_11",
	last_change_time = 1694165370,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_BrokenParts_12",
	last_change_time = 1694165373,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_Broken_01",
	last_change_time = 1692779822,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "lit",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "lit",
		'mesh', "lit",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_Broken_02",
	last_change_time = 1694165238,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "lit",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "lit",
		'mesh', "lit",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_Broken_03",
	last_change_time = 1694165242,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "lit",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "lit",
		'mesh', "lit",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_EMP",
	last_change_time = 1695195687,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "fly",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "land",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "takeOff",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_TrusterBack",
	last_change_time = 1693382292,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "lit",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "lit",
		'mesh', "lit",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_TrusterBack_EMP",
	last_change_time = 1695195732,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_TrusterFront",
	last_change_time = 1693382331,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "lit",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "lit",
		'mesh', "lit",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Droidship_Guardians_TrusterFront_EMP",
	last_change_time = 1695195755,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Max",
	group = "Default",
	id = "EnergySword",
	last_change_time = 1689240601,
	material_type = "Weapon",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Max",
	group = "Default",
	id = "HandGrenadeEMP",
	last_change_time = 1690278154,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "LandingCapsule_Guardians",
	last_change_time = 1690194904,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "fly",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "fly_EMP",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "lit",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "fly",
		'mesh', "fly",
	}),
	PlaceObj('StateSpec', {
		'name', "fly_EMP",
		'mesh', "fly_EMP",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "lit",
		'mesh', "lit",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "LandingCapsule",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "LandingCapsule_Guardians_EMP",
	last_change_time = 1695201941,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "fly",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "fly",
		'mesh', "fly",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "AutoAttachObject,AnimatedTextureObject",
	editor_exclude = true,
	fade_category = "Auto 600%",
	group = "Default",
	id = "MedicalBed",
	last_change_time = 1693299388,
	material_type = "Electronics",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "lit",
		'animated', true,
	}),
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "working_End",
		'mesh', "lit",
	}),
	PlaceObj('StateSpec', {
		'name', "working_Idle",
		'mesh', "lit",
	}),
	PlaceObj('StateSpec', {
		'name', "working_Start",
		'mesh', "lit",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Planet_Guardians",
	last_change_time = 1695624912,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "AutoAttachObject",
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "QuadrocopterCombat",
	last_change_time = 1692190048,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "working",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "death",
		'mesh', "working",
	}),
	PlaceObj('StateSpec', {
		'name', "death_Idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "fly",
		'mesh', "working",
	}),
	PlaceObj('StateSpec', {
		'name', "fly_Death",
		'mesh', "working",
	}),
	PlaceObj('StateSpec', {
		'name', "fly_to_Hover",
		'mesh', "working",
	}),
	PlaceObj('StateSpec', {
		'name', "hover_to_Fly",
		'mesh', "working",
	}),
	PlaceObj('StateSpec', {
		'name', "hover_to_Ground",
		'mesh', "working",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "working",
	}),
	PlaceObj('StateSpec', {
		'name', "walk",
		'mesh', "working",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "QuadrocopterCombat_Pad",
	last_change_time = 1692621971,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('MeshSpec', {
		'name', "working",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "working",
		'mesh', "working",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Prop",
	editor_subcategory = "Large",
	fade_category = "Never",
	group = "Default",
	id = "Quadrocopter_Gun",
	last_change_time = 1690197560,
	material_type = "Metal",
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "ResourceEntityClass",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "ResourceWeapon_EnergySword",
	last_change_time = 1689240649,
	material_type = "Weapon",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "RobotDummyStatic",
	last_change_time = 1700553527,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "AnimatedTextureObject",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "RobotStation",
	last_change_time = 1690882346,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('MeshSpec', {
		'name', "working",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "working",
		'mesh', "working",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "AnimatedTextureObject",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "RobotStationChef",
	last_change_time = 1692861704,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('MeshSpec', {
		'name', "working",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "working",
		'mesh', "working",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "AnimatedTextureObject",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "RobotStationCombat",
	last_change_time = 1692861684,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 2,
	}),
	PlaceObj('MeshSpec', {
		'name', "working",
		'lod', 2,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "working",
		'mesh', "working",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_01",
	last_change_time = 1694166394,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_02",
	last_change_time = 1694166406,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_03",
	last_change_time = 1694166411,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_04",
	last_change_time = 1694173738,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_05",
	last_change_time = 1694173740,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_06",
	last_change_time = 1694173742,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_07",
	last_change_time = 1694173743,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_08",
	last_change_time = 1694173746,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_09",
	last_change_time = 1694173750,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_10",
	last_change_time = 1694173752,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_11",
	last_change_time = 1700229327,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_12",
	last_change_time = 1700229331,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_13",
	last_change_time = 1700229333,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_14",
	last_change_time = 1700229336,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_15",
	last_change_time = 1700229338,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_16",
	last_change_time = 1700229340,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_17",
	last_change_time = 1700229342,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_18",
	last_change_time = 1700229345,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_19",
	last_change_time = 1700229347,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Interior",
	editor_category = "Prop",
	editor_subcategory = "Small",
	fade_category = "Auto 400%",
	group = "Default",
	id = "Robot_Combat_Destroyed_20",
	last_change_time = 1700229350,
	material_type = "Metal",
	placeholder = true,
	save_in = "Robots",
	status = "In production",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_F_Hope_Arms",
	inherit_entity = "HumanMale",
	last_change_time = 1693304011,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_F_Hope_Eyes",
	inherit_entity = "HumanMale",
	last_change_time = 1693307051,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_F_Hope_Feet",
	inherit_entity = "HumanMale",
	last_change_time = 1693304074,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_F_Hope_Hands",
	inherit_entity = "HumanMale",
	last_change_time = 1693304023,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "HumanHead",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_F_Hope_Head",
	inherit_entity = "HumanMale",
	last_change_time = 1693303934,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_F_Hope_Pelvis",
	inherit_entity = "HumanMale",
	last_change_time = 1693304039,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_F_Hope_Shoulders",
	inherit_entity = "HumanMale",
	last_change_time = 1693303986,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_F_Hope_Teeth",
	inherit_entity = "HumanMale",
	last_change_time = 1693307075,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_F_Hope_Thighs",
	inherit_entity = "HumanMale",
	last_change_time = 1693304050,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_F_Hope_Torso",
	inherit_entity = "HumanMale",
	last_change_time = 1693304004,
	material_type = "Flesh",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Chassis_Arms",
	inherit_entity = "HumanMale",
	last_change_time = 1691666426,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Chassis_Feet",
	inherit_entity = "HumanMale",
	last_change_time = 1691666509,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Chassis_Hands",
	inherit_entity = "HumanMale",
	last_change_time = 1691666472,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "HumanHead",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Chassis_Head",
	inherit_entity = "HumanMale",
	last_change_time = 1691666364,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Chassis_Pelvis",
	inherit_entity = "HumanMale",
	last_change_time = 1691666482,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Chassis_Shoulders",
	inherit_entity = "HumanMale",
	last_change_time = 1691666407,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Chassis_Tights",
	inherit_entity = "HumanMale",
	last_change_time = 1691666492,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Chassis_Torso",
	inherit_entity = "HumanMale",
	last_change_time = 1691666383,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat1_Arms",
	inherit_entity = "HumanMale",
	last_change_time = 1691755276,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat1_Feet",
	inherit_entity = "HumanMale",
	last_change_time = 1691755321,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat1_Hands",
	inherit_entity = "HumanMale",
	last_change_time = 1691755398,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "HumanHead",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat1_Head",
	inherit_entity = "HumanMale",
	last_change_time = 1691755436,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat1_Pelvis",
	inherit_entity = "HumanMale",
	last_change_time = 1691755465,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat1_Shoulders",
	inherit_entity = "HumanMale",
	last_change_time = 1691755496,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat1_Tights",
	inherit_entity = "HumanMale",
	last_change_time = 1691755556,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat1_Torso",
	inherit_entity = "HumanMale",
	last_change_time = 1691755601,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat2_Arms",
	inherit_entity = "HumanMale",
	last_change_time = 1691755305,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat2_Feet",
	inherit_entity = "HumanMale",
	last_change_time = 1691755358,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat2_Hands",
	inherit_entity = "HumanMale",
	last_change_time = 1691755415,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "HumanHead",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat2_Head",
	inherit_entity = "HumanMale",
	last_change_time = 1691755449,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat2_Pelvis",
	inherit_entity = "HumanMale",
	last_change_time = 1691755479,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat2_Shoulders",
	inherit_entity = "HumanMale",
	last_change_time = 1691755539,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat2_Tights",
	inherit_entity = "HumanMale",
	last_change_time = 1691755588,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Combat2_Torso",
	inherit_entity = "HumanMale",
	last_change_time = 1691755615,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Service_Arms",
	inherit_entity = "HumanMale",
	last_change_time = 1691751495,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Service_Feet",
	inherit_entity = "HumanMale",
	last_change_time = 1691751538,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Service_Hands",
	inherit_entity = "HumanMale",
	last_change_time = 1691751508,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "HumanHead",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Service_Head",
	inherit_entity = "HumanMale",
	last_change_time = 1691751429,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Service_Pelvis",
	inherit_entity = "HumanMale",
	last_change_time = 1691751515,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Service_Shoulders",
	inherit_entity = "HumanMale",
	last_change_time = 1691751488,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Service_Tights",
	inherit_entity = "HumanMale",
	last_change_time = 1691751529,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "Robot_M_Service_Torso",
	inherit_entity = "HumanMale",
	last_change_time = 1691751444,
	material_type = "Metal",
	save_in = "Robots",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 3,
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "_mesh",
		'mesh', "mesh",
	}),
})

end

OnMsg.DataPreprocess = ApplyArtSpec
OnMsg.ModsReloaded = ApplyArtSpec

