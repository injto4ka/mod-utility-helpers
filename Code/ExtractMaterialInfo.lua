function ExtractMaterialInfo(material)
	material = material or selo() or SelectedObj
	if IsValid(material) then
		material = GetStateMaterial(material:GetEntity(), material:GetState())
	end
	if type(material) ~= "string" then
		return
	end
	local sub_count = GetNumSubMaterials(material)
	local data = {
		material = material,
		count = sub_count,
	}
	for i=1,sub_count do
		data[i] = GetMaterialProperties(material, i - 1)
	end
	local data_meta = {
		__tostring = function(v)
			return string.format("Material %s", v.material)
		end,
	}
	return setmetatable(data, data_meta)
end