return {
PlaceObj('ModItemCode', {
	'name', "DbgBreak",
	'comment', "This is made obsolete by using breakpoints in VSCode",
	'NameColor', RGBA(165, 89, 89, 255),
	'CodeFileName', "Code/DbgBreak.lua",
}),
PlaceObj('ModItemCode', {
	'name', "DbgVisuals",
	'comment', "This is now officially supported in debug mode",
	'NameColor', RGBA(165, 89, 89, 255),
	'CodeFileName', "Code/DbgVisuals.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ExtractTranslation",
	'comment', "This is now officially supported in the mod editor",
	'NameColor', RGBA(165, 89, 89, 255),
	'CodeFileName', "Code/ExtractTranslation.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ExtractClassInfo",
	'NameColor', RGBA(119, 184, 211, 255),
	'CodeFileName', "Code/ExtractClassInfo.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ExtractEntityInfo",
	'NameColor', RGBA(119, 184, 211, 255),
	'CodeFileName', "Code/ExtractEntityInfo.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ExtractHierarchyInfo",
	'NameColor', RGBA(119, 184, 211, 255),
	'CodeFileName', "Code/ExtractHierarchyInfo.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ExtractMaterialInfo",
	'NameColor', RGBA(119, 184, 211, 255),
	'CodeFileName', "Code/ExtractMaterialInfo.lua",
}),
PlaceObj('ModItemCode', {
	'name', "FindCFunction",
	'comment', "Returns a C function name by address string",
	'CodeFileName', "Code/FindCFunction.lua",
}),
PlaceObj('ModItemCode', {
	'name', "TestMeshBlend",
	'comment', "Blend test for the selected object",
	'CodeFileName', "Code/TestMeshBlend.lua",
}),
PlaceObj('ModItemCode', {
	'name', "MarkedErrors",
	'NameColor', RGBA(237, 246, 177, 255),
	'CodeFileName', "Code/MarkedErrors.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ArtSpec-Common",
	'NameColor', RGBA(104, 158, 93, 255),
	'CodeFileName', "Code/ArtSpec-Common.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ArtSpec-base",
	'NameColor', RGBA(104, 158, 93, 255),
	'CodeFileName', "Code/ArtSpec-base.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ArtSpec-Preorder",
	'NameColor', RGBA(104, 158, 93, 255),
	'CodeFileName', "Code/ArtSpec-Preorder.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ArtSpec-Robots",
	'NameColor', RGBA(104, 158, 93, 255),
	'CodeFileName', "Code/ArtSpec-Robots.lua",
}),
}
