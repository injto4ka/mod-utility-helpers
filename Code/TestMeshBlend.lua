local skinny_weight = 50 * const.Scale.kg
local fat_weight = 90 * const.Scale.kg
	
function TestMeshBlend(torn_pct, dirty_pct, weight_pct, target)
	target = target or SelectedObj
	if not IsKindOf(target, "HumanVisuals") then
		return
	end
	local weight = weight_pct and (skinny_weight + MulDivRound(fat_weight - skinny_weight, weight_pct, 100))
	local orig_func = target.GetBodyPartBlendParams
	target.GetBodyPartBlendParams = function(...)
		local entity, weight0, torn_pct0, dirty_pct0 = orig_func(...)
		return entity, weight or weight0, torn_pct or torn_pct0, dirty_pct or dirty_pct0
	end
	target:ComposeBodyParts()
	target.GetBodyPartBlendParams = nil
end