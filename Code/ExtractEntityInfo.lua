function ExtractEntityInfo(obj)
	obj = obj or selo() or SelectedObj
	if not IsValid(obj) or not obj:HasEntity() then
		return
	end
	local data = {}
	local entity = obj:GetEntity()
	local state_idx = obj:GetState()
	local state_entity_owner = GetAnimEntity(entity, state_idx)
	data.entity = entity
	data.entity_bbox = GetEntityBoundingBox(entity)
	data.entity_vertices = obj:GetNumVertices()
	data.entity_tris = obj:GetNumTris()
	data.state_idx = state_idx
	data.state_name = obj:GetStateText()
	data.state_static = IsStaticAnim(entity, state_idx)
	data.state_duration = GetAnimDuration(entity, state_idx)
	local state_LOD_count = GetStateLODCount(entity, state_idx)
	local lods_meta = {
		__tostring = function(v)
			return string.format("%d %s", #v, #v == 1 and "lod" or "lods")
		end,
	}
	local lod_meta = {
		__tostring = function(v)
			return string.format("%d (%d) %s", v.dist, v.idx, v.material or "")
		end,
	}
	local state_LOD = {}
	for i=1,state_LOD_count do
		state_LOD[i] = setmetatable({
			idx = i - 1,
			dist = GetStateLODDistance(entity, state_idx, i - 1),
			material = GetStateMaterial(entity, state_idx, i - 1),
		}, lod_meta)
	end
	data.state_LOD = next(state_LOD) and setmetatable(state_LOD, lods_meta) or nil
	data.state_entity_owner = state_entity_owner
	local states_meta = {
		__tostring = function(v)
			return string.format("%d %s", #v, #v == 1 and "state" or "states")
		end,
	}
	local state_meta = {
		__tostring = function(v)
			return string.format("%s (%d)", v.name, v.idx)
		end,
	}
	local states = {}
	for i, state in pairs( EnumValidStates(entity) ) do
		states[i] = setmetatable({
			idx = state,
			name = GetStateName(state),
			file_mesh = GetStateMeshFile(entity, state),
			file_anim = GetStateAnimFile(entity, state),
			file_entity = GetEntityFile(entity),
			inherited_from = GetAnimEntity(entity, state)
		}, state_meta)
	end
	data.states = next(states) and setmetatable(states, states_meta) or nil
	local spots_meta = {
		__tostring = function(v)
			return string.format("%d %s", #v, #v == 1 and "spot" or "spots")
		end,
	}
	local spot_meta = {
		__tostring = function(v)
			return string.format("%s (%d) %s", v.name, v.idx, v.annotation or "")
		end,
	}
	local spots = {}
	local start_id, end_id = GetAllSpots(entity, state_idx)
	for i = start_id, end_id do
		local spot_annotation = obj:GetSpotAnnotation(i)
		local spot_name = obj:GetSpotName(i)
		local spot_pos, spot_axis, spot_angle = GetEntitySpotData(entity, i)
		table.insert(spots, setmetatable({
			idx = i,
			name = spot_name,
			annotation = spot_annotation,
			bone = GetSpotBone(entity, i),
			transform_scale = GetEntitySpotScale(entity, i),
			transform_pos = spot_pos,
			transform_axis = spot_axis,
			transform_angle = spot_angle,
		}, spot_meta))
	end
	data.spots = next(spots) and setmetatable(spots, spots_meta) or nil
	local tris_meta = {
		__tostring = function(v)
			return string.format("%d %s", #v, #v == 1 and "tri" or "tris")
		end,
	}
	local tri_meta = {
		__tostring = function(v)
			return string.format("%s %s %s", tostring(v[1]), tostring(v[2]), tostring(v[3]))
		end,
	}
	local collision = {}
	ForEachSurface(entity, state_idx, EntitySurfaces.Collision, function(pt1, pt2, pt3)
		collision[#collision + 1] = setmetatable({pt1, pt2, pt3}, tri_meta)
	end)
	data.collision_mesh = next(collision) and setmetatable(collision, tris_meta) or nil
	
	local data_meta = {
		__tostring = function(v)
			return string.format("Entity %s (%s)", v.entity, v.state_name)
		end,
	}
	return setmetatable(data, data_meta)
end