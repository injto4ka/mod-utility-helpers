if FirstLoad then
	ArtSpec_Common = false
end

local function ApplyArtSpec()

if ArtSpec_Common then return end
ArtSpec_Common = true

PlaceObj('EntitySpec', {
	editor_category = "Marker",
	editor_exclude = true,
	group = "Default",
	id = "Camera",
	last_change_time = 1584106336,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_category = "Marker",
	editor_exclude = true,
	group = "Default",
	id = "CameraTarget",
	last_change_time = 1584106339,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "Decal",
	editor_artset = "Common",
	editor_category = "Decal",
	editor_subcategory = "Terrain",
	group = "Default",
	id = "DecMod_Large",
	last_change_time = 1629884417,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "Decal",
	editor_artset = "Common",
	editor_category = "Decal",
	editor_subcategory = "Terrain",
	group = "Default",
	id = "DecMod_Medium",
	last_change_time = 1629884405,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "Decal",
	editor_artset = "Common",
	editor_category = "Decal",
	editor_subcategory = "Terrain",
	group = "Default",
	id = "DecMod_Small",
	last_change_time = 1629884346,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	group = "Default",
	id = "ErrorAnimatedMesh",
	last_change_time = 1679484028,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'animated', true,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	group = "Default",
	id = "ErrorStaticMesh",
	last_change_time = 1679484028,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "InvisibleObject",
	last_change_time = 1584106348,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
		'lod', 0,
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Markers",
	group = "Default",
	id = "MapAreaMarker",
	last_change_time = 1679484441,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Markers",
	fade_category = "Max",
	group = "Default",
	id = "MarkerMusic",
	last_change_time = 1590999192,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Markers",
	group = "Default",
	id = "NoteMarker",
	last_change_time = 1584690885,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Effects",
	fade_category = "Never",
	group = "Default",
	id = "ParticlePlaceholder",
	last_change_time = 1608123453,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Effects",
	fade_category = "Never",
	group = "Default",
	id = "ParticleSoundPlaceholder",
	last_change_time = 1608123450,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	DetailClass = "Eye Candy",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "PointLight",
	last_change_time = 1679485170,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_category = "Marker",
	editor_exclude = true,
	group = "Default",
	id = "RoomHelper",
	last_change_time = 1586323149,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_exclude = true,
	fade_category = "Never",
	id = "ShaderBall",
	last_change_time = 1679485446,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_category = "Marker",
	editor_exclude = true,
	fade_category = "Never",
	group = "Default",
	id = "SpotHelper",
	last_change_time = 1582201037,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "TerrainWaterObject",
	editor_artset = "Common",
	editor_category = "Markers",
	fade_category = "Never",
	group = "Default",
	id = "WaterFill",
	last_change_time = 1584106376,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	class_parent = "TerrainWaterObject",
	editor_artset = "Common",
	editor_category = "Markers",
	fade_category = "Never",
	group = "Default",
	id = "WaterFillBig",
	last_change_time = 1608103143,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Markers",
	fade_category = "Never",
	group = "Default",
	id = "WayPoint",
	last_change_time = 1584106381,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Markers",
	fade_category = "Never",
	group = "Default",
	id = "WayPointBig",
	last_change_time = 1584106384,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})

PlaceObj('EntitySpec', {
	editor_artset = "Common",
	editor_category = "Markers",
	fade_category = "Never",
	id = "WindMarker",
	last_change_time = 1600874491,
	save_in = "Common",
	status = "Ready",
	PlaceObj('MeshSpec', {
		'name', "mesh",
	}),
	PlaceObj('StateSpec', {
		'name', "idle",
		'mesh', "mesh",
	}),
})


LastCommonEntityID = 100029

EntityIDs = {
	Camera = 100001,
	CameraTarget = 100002,
	DecMod_Large = 100022,
	DecMod_Medium = 100023,
	DecMod_Small = 100024,
	ErrorAnimatedMesh = 100003,
	ErrorStaticMesh = 100013,
	InvisibleObject = 100005,
	MapAreaMarker = 100006,
	MarkerMusic = 100007,
	NoteMarker = 100016,
	ParticlePlaceholder = 100008,
	ParticleSoundPlaceholder = 100020,
	PointLight = 100009,
	RoomHelper = 100017,
	ShaderBall = 100014,
	SpotHelper = 100015,
	WaterFill = 100010,
	WaterFillBig = 100021,
	WayPoint = 100011,
	WayPointBig = 100012,
	WindMarker = 100019,
}

end

OnMsg.DataPreprocess = ApplyArtSpec
OnMsg.ModsReloaded = ApplyArtSpec
